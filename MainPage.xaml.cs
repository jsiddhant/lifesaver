﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

using Microsoft.Band;
using System.Threading.Tasks;
using Microsoft.Band.Tiles;
using Microsoft.Band.Tiles.Pages;
using Windows.UI;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Media.Animation;
using Microsoft.Band.Notifications;
using Microsoft.Band.Personalization;
using Windows.Phone.UI.Input;


namespace LifeSaver
{

    public sealed partial class MainPage : Page
    {
        bool first_time_navigation = true;

        bool Is_graph_visible = false;

        bool Is_connected = false;

        private IBandClient bandclient;

        int shift = 0;

        List<Point> heartbeat_data = new List<Point>();
        Storyboard storyboard1 = new Storyboard();

        DispatcherTimer timer = new DispatcherTimer();

        int point;
        int point_count = 0;
        int scaling_factor = 5;

       
        public MainPage()
        {
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Required;

            timer.Interval = System.TimeSpan.FromMilliseconds(1000);

            storyboard1.Completed += storyboard1_Completed;
            
            timer.Tick += timer_Tick; 

            HardwareButtons.BackPressed += HardwareButtons_BackPressed;
        }

        private void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            ;
        }

        async Task saveStringToLocalFile( string content)
        {
            string filename = "logs.txt";
            // saves the string 'content' to a file 'filename' in the app's local storage folder
            byte[] fileBytes = System.Text.Encoding.UTF8.GetBytes(content.ToCharArray());

            // create a file with the given filename in the local folder; replace any existing file with the same name
            StorageFile file = await Windows.Storage.ApplicationData.Current.LocalFolder.CreateFileAsync(filename, CreationCollisionOption.OpenIfExists);

            // write the char array created from the content string into the file
            using (var stream = await file.OpenStreamForWriteAsync())
            {
                stream.Write(fileBytes, 0, fileBytes.Length);
            }
        }

        public static async Task<string> readStringFromLocalFile()
        {
            // reads the contents of file 'filename' in the app's local storage folder and returns it as a string
            string filename = "logs.txt";
            // access the local folder
            StorageFolder local = Windows.Storage.ApplicationData.Current.LocalFolder;
            // open the file 'filename' for reading
            Stream stream = await local.OpenStreamForReadAsync(filename);
            string text;

            // copy the file contents into the string 'text'
            using (StreamReader reader = new StreamReader(stream))
            {
                text = reader.ReadToEnd();
            }

            return text;
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            LoadingAnimation();

            if (first_time_navigation == true)
            {

                first_time_navigation = false;

                if (e.NavigationMode != NavigationMode.New)
                {
                    return;
                }

                var bands = await BandClientManager.Instance.GetBandsAsync();
                var band = bands.FirstOrDefault();

                if (band == null)
                {
                    this.status.Text = "Band not found!";
                    return;
                }

                this.StatusMessage.Text = "Connecting....";
                this.status.Text = "Connecting....";
                this.bandclient = await BandClientManager.Instance.ConnectAsync(band);
                this.status.Text = "Connected!";
                this.StatusMessage.Text = "Connecting!";

                bool userconsentgranted;

                switch (this.bandclient.SensorManager.HeartRate.GetCurrentUserConsent())
                {
                    case UserConsent.Declined:
                        userconsentgranted = false;
                        break;

                    case UserConsent.Granted:
                        userconsentgranted = true;
                        break;

                    default:
                    case UserConsent.NotSpecified:
                        userconsentgranted = await this.bandclient.SensorManager.HeartRate.RequestUserConsentAsync();
                        break;
                }

                if (userconsentgranted)
                {
                    this.bandclient.SensorManager.HeartRate.ReadingChanged += async (sender, args) =>
                        {
                            await this.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal,
                                () => this.HR_reading.Text = string.Format("{0}", args.SensorReading.HeartRate));

                        };
                }
                else
                    this.status.Text = "User consent denied.";

                await this.CreateOrInitializeTile();

                await this.bandclient.SensorManager.HeartRate.StartReadingsAsync();

                Is_connected = true;
                this.StatusMessage.Text = "Aquiring Heartbeat.";

                Is_graph_visible = true;
                black.Visibility = Visibility.Collapsed;

                HeartBeatCircle.Visibility = Visibility.Visible;
                animation();
                timer.Start();
                graph(0);
            }
            else
            {
                if (e.Parameter == null)
                    this.Frame.Navigate(typeof(MainPage), "no");
            }
        }

        private async Task CreateOrInitializeTile()
        {
            Guid tileGuid = Guid.Parse("{857C016D-8F57-4A30-A714-0ABEEC9B51BD}");
            Guid pageGuid1 = Guid.Parse("{76D26A35-534F-4267-A8FA-CC2DC67821F8}");

            var tiles = await this.bandclient.TileManager.GetTilesAsync();

            BandTile tile = tiles.FirstOrDefault();

            if (tile == null)
            {
                var panel = new FlowPanel
                {
                    Rect = new PageRect(0, 0, 245, 106),
                    Orientation = FlowPanelOrientation.Horizontal,
                    HorizontalAlignment = Microsoft.Band.Tiles.Pages.HorizontalAlignment.Center,
                    VerticalAlignment = Microsoft.Band.Tiles.Pages.VerticalAlignment.Center,
                };

                panel.Elements.Add(
                    new TextButton
                    {
                        ElementId = 1,
                        Rect = new PageRect(0, 0, 200, 70),
                        PressedColor = Colors.Red.ToBandColor(),
                        HorizontalAlignment = Microsoft.Band.Tiles.Pages.HorizontalAlignment.Center,
                        VerticalAlignment = Microsoft.Band.Tiles.Pages.VerticalAlignment.Center,
                    }
                    );

                var layout = new PageLayout(panel);

                tile = new BandTile(tileGuid)
                {
                    Name = "LifeSaver",
                    TileIcon = await LoadIcon("ms-appx:///Assets/TileIcon.png"),
                    SmallIcon = await LoadIcon("ms-appx:///Assets/SmallIcon.png")
                };

                tile.PageLayouts.Add(layout);

                await this.bandclient.TileManager.AddTileAsync(tile);

                try
                {
                    BandTheme newtheme = new BandTheme()
                    {
                        Base = Colors.Firebrick.ToBandColor(),
                        HighContrast = Colors.IndianRed.ToBandColor(),
                        Highlight = Colors.Firebrick.ToBandColor(),
                        Lowlight = Colors.Tomato.ToBandColor(),
                        Muted = Colors.Firebrick.ToBandColor(),
                        SecondaryText = Colors.Tomato.ToBandColor(),
                    };
                    await bandclient.PersonalizationManager.SetThemeAsync(newtheme);
                }

                catch (Exception b)
                {
                    status.Text = b.Message;
                }

                try
                {
                    BandImage meTileImage = await LoadMeTileImage("ms-appx:///Assets/MeTile.png");
                    await bandclient.PersonalizationManager.SetMeTileImageAsync(meTileImage);
                }
                catch (Exception b)
                {
                    status.Text = b.Message;
                }

                try
                {
                    await bandclient.NotificationManager.SendMessageAsync(Guid.Parse("{857C016D-8F57-4A30-A714-0ABEEC9B51BD}"), "First Message", "Hi From LifeSaver!", DateTimeOffset.Now, MessageFlags.ShowDialog);
                }
                catch (Exception b)
                {
                    status.Text = b.Message;
                }
            }

            await this.bandclient.TileManager.SetPagesAsync(

                tileGuid,
                new PageData(
                    pageGuid1,
                    0, new TextButtonData(1, "Emergency Mode!"))
                );
        }

        private async Task<BandIcon> LoadIcon(string uri)
        {
            StorageFile imagefile = await StorageFile.GetFileFromApplicationUriAsync(new Uri(uri));

            using (IRandomAccessStream fileStream = await imagefile.OpenAsync(FileAccessMode.Read))
            {
                WriteableBitmap bitmap = new WriteableBitmap(1, 1);
                await bitmap.SetSourceAsync(fileStream);
                return bitmap.ToBandIcon();
            }
        }

        private async Task<BandImage> LoadMeTileImage(string uri)
        {
            StorageFile imagefile = await StorageFile.GetFileFromApplicationUriAsync(new Uri(uri));

            using (IRandomAccessStream fileStream = await imagefile.OpenAsync(FileAccessMode.Read))
            {
                WriteableBitmap bitmap = new WriteableBitmap(310, 102);
                await bitmap.SetSourceAsync(fileStream);
                return bitmap.ToBandImage();
            }
        }

        private async void storedata()
        {
            int i = 0;
            await saveStringToLocalFile("Log "+i+"reading :"+HR_reading.Text);
            var read = await readStringFromLocalFile();
            await Task.Delay(TimeSpan.FromSeconds(10));
            i++;
            storedata();
        }

        async void timer_Tick(object sender, object e)
        {
            int.TryParse(HR_reading.Text, out point);

            if (point <= 40)
            {
                storyboard1.Stop();
                timer.Stop();
                this.Frame.Navigate(typeof(SOS), null);
            }

           if(point>=95)
           {
               try
               {
                   await this.bandclient.NotificationManager.VibrateAsync(VibrationType.NotificationTwoTone);
               }
               catch (Exception b)
               {
                   status.Text = b.Message;
               }

               try
               {
                   await bandclient.NotificationManager.ShowDialogAsync(Guid.Parse("{857C016D-8F57-4A30-A714-0ABEEC9B51BD}"), "High Pulse!", "Are you in a state of unrest?");
               }
               catch (BandException b)
               {
                   status.Text = b.Message;
               }
           }

           if (point - 72 <= -30)
           { scaling_factor = 1; this.StatusMessage.Text = "Your hearbeat is abnormally low!!"; }
           else if (point - 72 >= 90)
           { scaling_factor = 1; this.StatusMessage.Text = "Your hearbeat is extremely high!!"; }
           else if (point - 72 >= 60)
           { scaling_factor = 2; this.StatusMessage.Text = "Your hearbeat is abnormally high!!"; }
           else if (point - 72 >= 30)
           { scaling_factor = 5; this.StatusMessage.Text = "Your hearbeat is bit high!"; }
            else
           { this.StatusMessage.Text = "Your hearbeat is good and under control!"; }

            heartbeat_data.Add(new Point(720 + 80 * point_count, 200 + (point-72)*scaling_factor));           
            
            point_count++;
        }

        async void animation()
        {
            while (true)
            {
                i1.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i1.Visibility = Visibility.Collapsed;
                i2.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i2.Visibility = Visibility.Collapsed;
                i3.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i3.Visibility = Visibility.Collapsed;
                i4.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i4.Visibility = Visibility.Collapsed;
                i5.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i5.Visibility = Visibility.Collapsed;
                i6.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i6.Visibility = Visibility.Collapsed;
                i7.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i7.Visibility = Visibility.Collapsed;
                i8.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i8.Visibility = Visibility.Collapsed;
                i9.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i9.Visibility = Visibility.Collapsed;
                i10.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i10.Visibility = Visibility.Collapsed;
                i11.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i11.Visibility = Visibility.Collapsed;
                i12.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i12.Visibility = Visibility.Collapsed;
                i13.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i13.Visibility = Visibility.Collapsed;
                i14.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i14.Visibility = Visibility.Collapsed;
                i15.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i15.Visibility = Visibility.Collapsed;
                i16.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i16.Visibility = Visibility.Collapsed;
                i17.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i17.Visibility = Visibility.Collapsed;
                i18.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i18.Visibility = Visibility.Collapsed;
                i19.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i19.Visibility = Visibility.Collapsed;
                i20.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i20.Visibility = Visibility.Collapsed;
                i21.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i21.Visibility = Visibility.Collapsed;
                i22.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i22.Visibility = Visibility.Collapsed;
                i23.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i23.Visibility = Visibility.Collapsed;
                i24.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i24.Visibility = Visibility.Collapsed;
                i25.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i25.Visibility = Visibility.Collapsed;
                i26.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i26.Visibility = Visibility.Collapsed;
                i27.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i27.Visibility = Visibility.Collapsed;
                i28.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i28.Visibility = Visibility.Collapsed;
                i29.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i29.Visibility = Visibility.Collapsed;
                i30.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i30.Visibility = Visibility.Collapsed;
                i31.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i31.Visibility = Visibility.Collapsed;
                i32.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i32.Visibility = Visibility.Collapsed;
                i33.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i33.Visibility = Visibility.Collapsed;
                i34.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i34.Visibility = Visibility.Collapsed;
                i35.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i35.Visibility = Visibility.Collapsed;
                i36.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i36.Visibility = Visibility.Collapsed;
                i37.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i37.Visibility = Visibility.Collapsed;
                i38.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i38.Visibility = Visibility.Collapsed;
                i39.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i39.Visibility = Visibility.Collapsed;
                i40.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i40.Visibility = Visibility.Collapsed;
                i41.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i41.Visibility = Visibility.Collapsed;
                i42.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i42.Visibility = Visibility.Collapsed;
                i43.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i43.Visibility = Visibility.Collapsed;
                i44.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i44.Visibility = Visibility.Collapsed;
                i45.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i45.Visibility = Visibility.Collapsed;
                i46.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i46.Visibility = Visibility.Collapsed;
                i47.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i47.Visibility = Visibility.Collapsed;
                i48.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i48.Visibility = Visibility.Collapsed;
                i49.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i49.Visibility = Visibility.Collapsed;
                i50.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i50.Visibility = Visibility.Collapsed;
                i51.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i51.Visibility = Visibility.Collapsed;
                i52.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i52.Visibility = Visibility.Collapsed;
                i53.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i53.Visibility = Visibility.Collapsed;
                i54.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i54.Visibility = Visibility.Collapsed;
                i55.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i55.Visibility = Visibility.Collapsed;
                i56.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i56.Visibility = Visibility.Collapsed;
                i57.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i57.Visibility = Visibility.Collapsed;
                i58.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i58.Visibility = Visibility.Collapsed;
                i59.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i59.Visibility = Visibility.Collapsed;
                i60.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i60.Visibility = Visibility.Collapsed;
                i61.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i61.Visibility = Visibility.Collapsed;
                i62.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i62.Visibility = Visibility.Collapsed;
                i63.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i63.Visibility = Visibility.Collapsed;
                i64.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i64.Visibility = Visibility.Collapsed;
                i65.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i65.Visibility = Visibility.Collapsed;
                i66.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i66.Visibility = Visibility.Collapsed;
                i67.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i67.Visibility = Visibility.Collapsed;
                i68.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i68.Visibility = Visibility.Collapsed;
                i69.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i69.Visibility = Visibility.Collapsed;
                i70.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i70.Visibility = Visibility.Collapsed;
                i71.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i71.Visibility = Visibility.Collapsed;
                i72.Visibility = Visibility.Visible;
                await Task.Delay(25);
                i72.Visibility = Visibility.Collapsed;

               if (Is_graph_visible != true)
                {
                    black.Visibility = Visibility.Visible;
                    return;
                }
            }
        }

        void graph(int x)
        {
            DoubleAnimation doubleanimation1 = new DoubleAnimation()
            {
                From = x + 420,
                To = x + 500,
                Duration = new Duration(TimeSpan.FromMilliseconds(1000)),
            };

            Storyboard.SetTarget(doubleanimation1, l1);
            Storyboard.SetTargetProperty(doubleanimation1, "(Canvas.Left)");
            storyboard1.Children.Add(doubleanimation1);

            storyboard1.Begin();
        }

        void storyboard1_Completed(object sender, object e)
        {
            try
            {
                l1.Points.Add(heartbeat_data[0]);
                heartbeat_data.RemoveAt(0);
                l1.Points.RemoveAt(0);
                storyboard1.Stop();
                storyboard1.Children.Clear();
                shift++;
                graph(80 * shift);
            }

            catch (ArgumentOutOfRangeException)
            {
                l1.Points.RemoveAt(0);
                storyboard1.Stop();
                storyboard1.Children.Clear();
                shift++;
                graph(80 * shift);
            }
        }

        private void Dashboard_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(Dashboard), null);
        }

        async void LoadingAnimation()
        {
            while (true)
            {
                a1.Visibility = Visibility.Visible;
                await Task.Delay(100);
                a1.Visibility = Visibility.Collapsed;
                a2.Visibility = Visibility.Visible;
                await Task.Delay(100);
                a2.Visibility = Visibility.Collapsed;
                a3.Visibility = Visibility.Visible;
                await Task.Delay(100);
                a3.Visibility = Visibility.Collapsed;
                a4.Visibility = Visibility.Visible;
                await Task.Delay(100);
                a4.Visibility = Visibility.Collapsed;
                a5.Visibility = Visibility.Visible;
                await Task.Delay(100);
                a5.Visibility = Visibility.Collapsed;
                a6.Visibility = Visibility.Visible;
                await Task.Delay(100);
                a6.Visibility = Visibility.Collapsed;
                a7.Visibility = Visibility.Visible;
                await Task.Delay(100);
                a7.Visibility = Visibility.Collapsed;
                a8.Visibility = Visibility.Visible;
                await Task.Delay(100);
                a8.Visibility = Visibility.Collapsed;

                if (Is_connected == true)
                {
                    return;
                }
            }
        }
    
    }
}
