﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LifeSaver
{
    public class Profile
    {
      public  string Name { get; set; }
      public string Age { get; set; }
      public string PhoneNo { get; set; }
      public string EmergencyNo1 { get; set; }
      public string EmergencyNo2 { get; set; }
    }
}
