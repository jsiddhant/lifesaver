﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace LifeSaver
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class UserProfile : Page
    {
        public UserProfile()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void TextBlock_SelectionChanged(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Windows.Storage.ApplicationDataContainer localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
            string phnNo = (string)localSettings.Values["PhoneNo"].ToString();
            if(string.Compare(phnNo,phoneNo.Text)<0)
            {
                var dialog = new MessageDialog("Phone number doesn't match !");
                dialog.Commands.Add(new UICommand("OK"));
                dialog.ShowAsync();
            }
            Windows.Storage.ApplicationDataCompositeValue composite =
   (Windows.Storage.ApplicationDataCompositeValue)localSettings.Values["AuthSettings"];
           localSettings.Values["Name"] = name.Text;
           localSettings.Values["PhoneNo"] = phoneNo.Text;
           localSettings.Values["Age"] = Age.Text;
           localSettings.Values["EmergencyContact1"] = emergencyNumber1.Text;
           localSettings.Values["EmergencyContact2"] = emergencyNumber2.Text;
            this.Frame.Navigate(typeof(MainPage));
        }

        private void reset_Click(object sender, RoutedEventArgs e)
        {
            name.Text = string.Empty;
            Age.Text = string.Empty;
            phoneNo.Text = string.Empty;
            emergencyNumber1.Text = string.Empty;
            emergencyNumber2.Text = string.Empty;
           
        }

        private void TextBlock_SelectionChanged_2(object sender, RoutedEventArgs e)
        {

        }

        private void TextBlock_SelectionChanged_1(object sender, RoutedEventArgs e)
        {

        }

       

       

       

       
    }
}
