﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Networking.PushNotifications;
using Microsoft.WindowsAzure.Messaging;
using Windows.UI.Popups;
using Windows.Storage;
using System.Net.Http;
using System.Threading.Tasks;
using System.Net.Http.Headers;

namespace LifeSaver
{
   
    public sealed partial class StartPage : Page
    {
        bool Is_loaded = false;

        private static string BACKEND_ENDPOINT = "http://kshitij1995.azurewebsites.net";
        public StartPage()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }
       
        private async void LoginAndRegisterClick(object sender, RoutedEventArgs e)
        {
            LoadingAnimation();
          //  LoginButton.IsEnabled = false;
            SetAuthenticationTokenInLocalStorage();
            ApplicationDataContainer localSettings = ApplicationData.Current.LocalSettings;
            ApplicationDataCompositeValue composite = new ApplicationDataCompositeValue();
            var channel = await PushNotificationChannelManager.CreatePushNotificationChannelForApplicationAsync();

            try
            {
                await new RegisterClient(BACKEND_ENDPOINT).RegisterAsync(channel.Uri, new string[] { "myTag" });

                var dialog = new MessageDialog("Registered as: " + UsernameTextBox.Text);
                dialog.Commands.Add(new UICommand("OK"));
                 dialog.ShowAsync();
                 Is_loaded = true;

                localSettings.Values["IsRegistered"] = true;
                
                this.Frame.Navigate(typeof(EditProfile));
            }
            catch (Exception ex)
            {

                localSettings.Values["IsRegistered"] = false;
                MessageDialog alert = new MessageDialog(ex.Message, "Failed to register with RegisterClient");
                alert.ShowAsync();
                LoginButton.IsEnabled = true;
                Is_loaded = true;
            }

        }

        private void SetAuthenticationTokenInLocalStorage()
        {
            string username = phoneNo.Text;
            string password = PasswordTextBox.Password;

            var token = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(username + ":" + password));
            ApplicationData.Current.LocalSettings.Values["AuthenticationToken"] = token;
            ApplicationData.Current.LocalSettings.Values["PhoneNo"] = username;
            ApplicationData.Current.LocalSettings.Values["example"] = "test";
           ApplicationDataContainer localSettings = ApplicationData.Current.LocalSettings;
           StorageFolder localFolder = ApplicationData.Current.LocalFolder;
           ApplicationDataCompositeValue composite = new ApplicationDataCompositeValue();
            composite["AuthenticationToken"] = token;
            composite["username"] = username;

            localSettings.Values["AuthSettings"] = composite;

        }

        private void TextBlock_SelectionChanged(object sender, RoutedEventArgs e)
        {

        }

        async void LoadingAnimation()
        {
            while (true)
            {
                a1.Visibility = Visibility.Visible;
                await Task.Delay(100);
                a1.Visibility = Visibility.Collapsed;
                a2.Visibility = Visibility.Visible;
                await Task.Delay(100);
                a2.Visibility = Visibility.Collapsed;
                a3.Visibility = Visibility.Visible;
                await Task.Delay(100);
                a3.Visibility = Visibility.Collapsed;
                a4.Visibility = Visibility.Visible;
                await Task.Delay(100);
                a4.Visibility = Visibility.Collapsed;
                a5.Visibility = Visibility.Visible;
                await Task.Delay(100);
                a5.Visibility = Visibility.Collapsed;
                a6.Visibility = Visibility.Visible;
                await Task.Delay(100);
                a6.Visibility = Visibility.Collapsed;
                a7.Visibility = Visibility.Visible;
                await Task.Delay(100);
                a7.Visibility = Visibility.Collapsed;
                a8.Visibility = Visibility.Visible;
                await Task.Delay(100);
                a8.Visibility = Visibility.Collapsed;

                if (Is_loaded == true)
                {
                    return;
                }
            }
        }
    }
}
