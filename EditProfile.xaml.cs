﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Phone.UI.Input;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace LifeSaver
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class EditProfile : Page
    {
        Profile client;
        ApplicationDataContainer localSettings;
        public EditProfile()
        {
            this.InitializeComponent();
            client = new Profile();
            localSettings = ApplicationData.Current.LocalSettings;
            HardwareButtons.BackPressed += HardwareButtons_BackPressed;
        }

        private void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            ;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void TextBlock_SelectionChanged(object sender, RoutedEventArgs e)
        {

        }

        
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            bool edit = Convert.ToBoolean( localSettings.Values["edit"]);
           
            client.Name=name.Text;
            client.PhoneNo=phoneNo.Text;
            client.Age=Age.Text;
            client.EmergencyNo1=emergencyNumber1.Text;
            client.EmergencyNo2 = emergencyNumber2.Text;

            
            localSettings.Values["Name"] = client.Name;
            localSettings.Values["PhoneNo"] = client.PhoneNo;
            localSettings.Values["Age"] = client.Age;
            localSettings.Values["EmergencyContact1"] = client.EmergencyNo1;
            localSettings.Values["EmergencyContact2"] = client.EmergencyNo2;
            if (edit)
            {
                localSettings.Values.Remove("edit");
                this.Frame.Navigate(typeof(Dashboard),"profile");
            }
            else
            {
                this.Frame.Navigate(typeof(MainPage), null);
            }
        }

        private bool phoneNoCompare(string number)
        {
            string phnNo = (string)localSettings.Values["PhoneNo"].ToString();
            if (string.Compare(phnNo, number) < 0)
            {
                var dialog = new MessageDialog("Phone number doesn't match !");
                dialog.Commands.Add(new UICommand("OK"));
                dialog.ShowAsync();
                return false;
            }
            return true;
        }

        private void reset_Click(object sender, RoutedEventArgs e)
        {
            name.Text = string.Empty;
            Age.Text = string.Empty;
            phoneNo.Text = string.Empty;
            emergencyNumber1.Text = string.Empty;
            emergencyNumber2.Text = string.Empty;

        }

        private void TextBlock_SelectionChanged_2(object sender, RoutedEventArgs e)
        {

        }

        private void TextBlock_SelectionChanged_1(object sender, RoutedEventArgs e)
        {

        }








    }
}
