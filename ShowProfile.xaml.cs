﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace LifeSaver
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ShowProfile : Page
    {
        ApplicationDataContainer localSettings;
        public ShowProfile()
        {
            this.InitializeComponent();
            localSettings = ApplicationData.Current.LocalSettings;
        }

     
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            Initialize();
        }

        private void Initialize()
        {
            
            string phnNo = (string)localSettings.Values["PhoneNo"].ToString();
            string nam = (string)localSettings.Values["Name"].ToString();
            string ag=(string)localSettings.Values["Age"].ToString();
            string emeNo1 = (string)localSettings.Values["EmergencyContact1"].ToString();
            string emeNo2 = (string)localSettings.Values["EmergencyContact2"].ToString();
            phoneNo.Text = phnNo;
            
            name.Text = nam;
            Age.Text = ag;
            emergencyNumber1.Text = emeNo1;
            emergencyNumber2.Text = emeNo2;

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            localSettings.Values["edit"]=true;
            this.Frame.Navigate(typeof(EditProfile),null);
        }

        private void TextBlock_SelectionChanged_2(object sender, RoutedEventArgs e)
        {

        }

        private void TextBlock_SelectionChanged(object sender, RoutedEventArgs e)
        {

        }

        private void TextBlock_SelectionChanged_1(object sender, RoutedEventArgs e)
        {

        }
    }
}
