﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;


namespace LifeSaver
{
   
    public sealed partial class SOS : Page
    {

        public string Latitudevalue, Longitutdevalue;

        bool Help_Button_click = false;

        private static string BACKEND_ENDPOINT = "http://kshitij1995.azurewebsites.net";
        
        public SOS()
        {
            this.InitializeComponent();
        }
        
        private static string DecodeBinaryBase64(string stringToDecode)
        {
            StringBuilder builder = new StringBuilder();
            foreach (var b in Convert.FromBase64String(stringToDecode))
                builder.Append(string.Format("{0:X2}", b));
            return "0x" + builder.ToString();
        }
        
        public static string Base64Decode(string base64EncodedData,int c)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes,0,c);
        }


        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            SOS_animation();
            
            ApplicationDataContainer localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;

            string name = (string)localSettings.Values["Name"].ToString();
            
            var message=" Rajan"+" might have suffered from Cardiac Arrest";

            var settings = ApplicationData.Current.LocalSettings.Values;

            string x = (string)settings["AuthenticationToken"];
            byte[] data = Convert.FromBase64String(x);
            var p = Base64Decode(x, data.Length);
            
            string eme = (string)localSettings.Values["EmergencyContact1"].ToString();
            string eme1 = (string)localSettings.Values["EmergencyContact2"].ToString();

            Emergency_contacts.Text = eme + "\n" + eme1;
            
            try
            {
                await sendPush("56",message);
                await sendPush("", message);

            }
            catch (Exception ex)
            {
                MessageDialog alert = new MessageDialog(ex.Message, "Exception-push");
                
            }
            
            Geolocator geolocator = new Geolocator();
            geolocator.DesiredAccuracyInMeters = 50;

            try
            {

                Geoposition geoposition = await geolocator.GetGeopositionAsync();
 
                Latitudevalue = geoposition.Coordinate.Latitude.ToString("0.00");
                Longitutdevalue = geoposition.Coordinate.Longitude.ToString("0.00");
            }
            catch (Exception ex)
            {
                MessageDialog alert = new MessageDialog(ex.Message, "Exception"); 
            }
            try
            {
                results();
            }
            catch (Exception ex)
            {
                MessageDialog alert = new MessageDialog(ex.Message, "Exception-result");
            }
        }

        async void results()
        {
            var data = await Initial_hospital_data();
            var place_id = Extract_place_id(data);
            List_of_all_nearby_hospitals(place_id);
        }

        async Task<string> Initial_hospital_data()
        {
            try
            {
                string link = "https://maps.googleapis.com/maps/api/place/search/json?key=AIzaSyA0ebpHgzdSOZOea7_VZwU3G6VUvbKddZY&location=" + Latitudevalue + "," + Longitutdevalue + "&radius=5000&keyword=hospital&sensor=true";
                Uri url = new Uri(link);
                HttpClient client = new HttpClient();
                var result = await client.GetStringAsync(url);
                return result.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        string[] Extract_place_id(string data)
        {
            int i = 1;
            int[] place_id_index = new int[50];
            string[] place_id = new string[50];
            place_id_index[0] = data.IndexOf("place_id");

            for (int pos = place_id_index[0] + 13; data[pos] != '\"'; pos++)
            {
                place_id[0] += data[pos];
            }

            while (place_id_index[i - 1] >= 0)
            {
                place_id_index[i] = data.IndexOf("place_id", place_id_index[i - 1] + 10);
                for (int pos = place_id_index[i] + 13; data[pos] != '\"'; pos++)
                {
                    place_id[i] += data[pos];
                }
                i++;
            }

            return place_id;
        }

        async Task<string> Hospital_name_phone(string place_id)
        {
            try
            {
                string link = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + place_id + "&key=AIzaSyA0ebpHgzdSOZOea7_VZwU3G6VUvbKddZY";
                Uri url = new Uri(link);
                HttpClient client = new HttpClient();
                var result = await client.GetStringAsync(url);
                return result.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        string Extract_name_phone_number_of_single_hospital(string Single_hospital_data)
        {
            int name_index = Single_hospital_data.IndexOf("\"name\"");
            int phone_index = Single_hospital_data.IndexOf("international_phone_number");
            string hospital_data = "";
            for (int i = name_index + 10; Single_hospital_data[i] != '\"'; i++)
            {
                hospital_data += Single_hospital_data[i];
            }
            hospital_data += "\n";
            for (int i = phone_index + 31; Single_hospital_data[i] != '\"'; i++)
            {
                hospital_data += Single_hospital_data[i];
            }
            return hospital_data;
        }

        async void List_of_all_nearby_hospitals(string[] place_id)
        {
            string final_hospitals_list = "";
            int loop = place_id.Length;
            for (int i = 0; i <= 3; i++)
            {
                var This_hospital_data = await Hospital_name_phone(place_id[i]);
                final_hospitals_list += Extract_name_phone_number_of_single_hospital(This_hospital_data) + "\n\n";
            }

           Hospital_list.Text = final_hospitals_list;
        }

        private async Task sendPush(string userTag, string message)
        {
            var POST_URL = BACKEND_ENDPOINT + "/api/notifications?pns=" +
                "wns" + "&to_tag=" + userTag;

            using (var httpClient = new HttpClient())
            {
                var settings = ApplicationData.Current.LocalSettings.Values;
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", (string)settings["AuthenticationToken"]);

                try
                {
                    await httpClient.PostAsync(POST_URL, new StringContent("\"" + message + "\"",
                        System.Text.Encoding.UTF8, "application/json"));
                }
                catch (Exception ex)
                {
                    MessageDialog alert = new MessageDialog(ex.Message, "Failed to send " + "wns" + " message");
                    alert.ShowAsync();
                }
            }
        }

        async void CPR_animation()
        {
            while (true)
            {
                CPR1.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR1.Visibility = Visibility.Collapsed;
                CPR2.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR2.Visibility = Visibility.Collapsed;
                CPR3.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR3.Visibility = Visibility.Collapsed;
                CPR4.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR4.Visibility = Visibility.Collapsed;
                CPR5.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR5.Visibility = Visibility.Collapsed;
                CPR6.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR6.Visibility = Visibility.Collapsed;
                CPR7.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR7.Visibility = Visibility.Collapsed;
                CPR8.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR8.Visibility = Visibility.Collapsed;
                CPR9.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR9.Visibility = Visibility.Collapsed;
                CPR10.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR10.Visibility = Visibility.Collapsed;
                CPR11.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR11.Visibility = Visibility.Collapsed;
                CPR12.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR12.Visibility = Visibility.Collapsed;
                CPR13.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR13.Visibility = Visibility.Collapsed;
                CPR14.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR14.Visibility = Visibility.Collapsed;
                CPR15.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR15.Visibility = Visibility.Collapsed;
                CPR16.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR16.Visibility = Visibility.Collapsed;
                CPR17.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR17.Visibility = Visibility.Collapsed;
                CPR18.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR18.Visibility = Visibility.Collapsed;
                CPR19.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR19.Visibility = Visibility.Collapsed;
                CPR20.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR20.Visibility = Visibility.Collapsed;
                CPR21.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR21.Visibility = Visibility.Collapsed;
                CPR22.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR22.Visibility = Visibility.Collapsed;
                CPR23.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR23.Visibility = Visibility.Collapsed;
                CPR24.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR24.Visibility = Visibility.Collapsed;
                CPR25.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR25.Visibility = Visibility.Collapsed;
                CPR26.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR26.Visibility = Visibility.Collapsed;
                CPR27.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR27.Visibility = Visibility.Collapsed;
                CPR28.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR28.Visibility = Visibility.Collapsed;
                CPR29.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR29.Visibility = Visibility.Collapsed;
                CPR30.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR30.Visibility = Visibility.Collapsed;
                CPR31.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR31.Visibility = Visibility.Collapsed;
                CPR32.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR32.Visibility = Visibility.Collapsed;
                CPR33.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR33.Visibility = Visibility.Collapsed;
                CPR34.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR34.Visibility = Visibility.Collapsed;
                CPR35.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR35.Visibility = Visibility.Collapsed;
                CPR36.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR36.Visibility = Visibility.Collapsed;
                CPR37.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR37.Visibility = Visibility.Collapsed;
                CPR38.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR38.Visibility = Visibility.Collapsed;
                CPR39.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR39.Visibility = Visibility.Collapsed;
                CPR40.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR40.Visibility = Visibility.Collapsed;
                CPR41.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR41.Visibility = Visibility.Collapsed;
                CPR42.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR42.Visibility = Visibility.Collapsed;
                CPR43.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR43.Visibility = Visibility.Collapsed;
                CPR44.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR44.Visibility = Visibility.Collapsed;
                CPR45.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR45.Visibility = Visibility.Collapsed;
                CPR46.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR46.Visibility = Visibility.Collapsed;
                CPR47.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR47.Visibility = Visibility.Collapsed;
                CPR48.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR48.Visibility = Visibility.Collapsed;
                CPR49.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR49.Visibility = Visibility.Collapsed;
                CPR50.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR50.Visibility = Visibility.Collapsed;
                CPR51.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR51.Visibility = Visibility.Collapsed;
                CPR52.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR52.Visibility = Visibility.Collapsed;
                CPR53.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR53.Visibility = Visibility.Collapsed;
                CPR54.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR54.Visibility = Visibility.Collapsed;
                CPR55.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR55.Visibility = Visibility.Collapsed;
                CPR56.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR56.Visibility = Visibility.Collapsed;
                CPR57.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR57.Visibility = Visibility.Collapsed;
                CPR58.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR58.Visibility = Visibility.Collapsed;
                CPR59.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR59.Visibility = Visibility.Collapsed;
                CPR60.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR60.Visibility = Visibility.Collapsed;
                CPR61.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR61.Visibility = Visibility.Collapsed;
                CPR62.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR62.Visibility = Visibility.Collapsed;
                CPR63.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR63.Visibility = Visibility.Collapsed;
                CPR64.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR64.Visibility = Visibility.Collapsed;
                CPR65.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR65.Visibility = Visibility.Collapsed;
                CPR66.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR66.Visibility = Visibility.Collapsed;
                CPR67.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR67.Visibility = Visibility.Collapsed;
                CPR68.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR68.Visibility = Visibility.Collapsed;
                CPR69.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR69.Visibility = Visibility.Collapsed;
                CPR70.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR70.Visibility = Visibility.Collapsed;
                CPR71.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR71.Visibility = Visibility.Collapsed;
                CPR72.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR72.Visibility = Visibility.Collapsed;
                CPR73.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR73.Visibility = Visibility.Collapsed;
                CPR74.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR74.Visibility = Visibility.Collapsed;
                CPR75.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR75.Visibility = Visibility.Collapsed;
                CPR76.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR76.Visibility = Visibility.Collapsed;
                CPR77.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR77.Visibility = Visibility.Collapsed;
                CPR78.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR78.Visibility = Visibility.Collapsed;
                CPR79.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR79.Visibility = Visibility.Collapsed;
                CPR80.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR80.Visibility = Visibility.Collapsed;
                CPR81.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR81.Visibility = Visibility.Collapsed;
                CPR82.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR82.Visibility = Visibility.Collapsed;
                CPR83.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR83.Visibility = Visibility.Collapsed;
                CPR84.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR84.Visibility = Visibility.Collapsed;
                CPR85.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR85.Visibility = Visibility.Collapsed;
                CPR86.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR86.Visibility = Visibility.Collapsed;
                CPR87.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR87.Visibility = Visibility.Collapsed;
                CPR88.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR88.Visibility = Visibility.Collapsed;
                CPR89.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR89.Visibility = Visibility.Collapsed;
                CPR90.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR90.Visibility = Visibility.Collapsed;
                CPR91.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR91.Visibility = Visibility.Collapsed;
                CPR92.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR92.Visibility = Visibility.Collapsed;
                CPR93.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR93.Visibility = Visibility.Collapsed;
                CPR94.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR94.Visibility = Visibility.Collapsed;
                CPR95.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR95.Visibility = Visibility.Collapsed;
                CPR96.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR96.Visibility = Visibility.Collapsed;
                CPR97.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR97.Visibility = Visibility.Collapsed;
                CPR98.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR98.Visibility = Visibility.Collapsed;
                CPR99.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR99.Visibility = Visibility.Collapsed;
                CPR100.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR100.Visibility = Visibility.Collapsed;
                CPR101.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR101.Visibility = Visibility.Collapsed;
                CPR102.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR102.Visibility = Visibility.Collapsed;
                CPR103.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR103.Visibility = Visibility.Collapsed;
                CPR104.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR104.Visibility = Visibility.Collapsed;
                CPR105.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR105.Visibility = Visibility.Collapsed;
                CPR106.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR106.Visibility = Visibility.Collapsed;
                CPR107.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR107.Visibility = Visibility.Collapsed;
                CPR108.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR108.Visibility = Visibility.Collapsed;
                CPR109.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR109.Visibility = Visibility.Collapsed;
                CPR110.Visibility = Visibility.Visible;
                await Task.Delay(50);
                CPR110.Visibility = Visibility.Collapsed;

            }
        }

        async void SOS_animation()
        {

            try
            {
                while (true)
                {

                    SOS_Image_2.Visibility = Visibility.Visible;
                    await Task.Delay(250);
                    SOS_Image_2.Visibility = Visibility.Collapsed;
                    SOS_Image_1.Visibility = Visibility.Visible;
                    await Task.Delay(250);
                    SOS_Image_1.Visibility = Visibility.Collapsed;

                    if (Help_Button_click == true)
                    {
                        SOS_Image_1.Visibility = Visibility.Collapsed;
                        SOS_Image_2.Visibility = Visibility.Collapsed;
                        CPR_animation();
                        return;
                    }
                }
            }

            catch (Exception ex)
            {
                MessageDialog Error = new MessageDialog(ex.Message, "CPR Animation Error");
                Error.ShowAsync();
            }
        }

        private void Help_Button_Click_1(object sender, RoutedEventArgs e)
        {
            Help_Button_click = true;
            Help_Button.Visibility = Visibility.Collapsed;
            Help_me_scream.Stop();
            CPR_metronome.Play();
            CPR_instructions_animation();
        }

        async void CPR_instructions_animation()
        {
            while (true)
            {

                CPR_Instructions1.Visibility = Visibility.Visible;
                await Task.Delay(2500);
                CPR_Instructions1.Visibility = Visibility.Collapsed;
                CPR_Instructions2.Visibility = Visibility.Visible;
                await Task.Delay(2500);
                CPR_Instructions2.Visibility = Visibility.Collapsed;
                CPR_Instructions3.Visibility = Visibility.Visible;
                await Task.Delay(2500);
                CPR_Instructions3.Visibility = Visibility.Collapsed;

            }
        }
   
    }
}
